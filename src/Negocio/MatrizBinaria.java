/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author EQUIPO
 */
public class MatrizBinaria implements OperacionMatriz{
    
    private boolean m[][];
    
    public MatrizBinaria(String datos)throws Exception{
        String x[]=datos.split(";");
        this.m= new boolean [x.length][];
        for(int i=0; i<x.length; i++){
            String z[]=x[i].split(",");
            this.m[i]=new boolean[z.length];
            
            for(int j=0; j<z.length; j++){
                if(z[j]=="1"){
                   boolean aux= true;
                    this.m[i][j]=aux;
                } else { if(z[j]=="0"){
                    boolean aux = false;
                    this.m[i][j]=aux;
                } else{
                    throw new Exception("Datos diferentes de 1 y 0");
                    }
                }
            }
        }
    }
    
    @Override
    public boolean isDispersa()throws Exception{
        if(this.m==null){
            throw new Exception("El vector esta vacio");
        }
        int tamaño;
        boolean aux=false;
        for(int i=1; i<this.m.length; i++){
            tamaño=this.m[i-1].length;
            
            if(this.m.length!=tamaño){
                aux= true;
                break;
            }
        }
        return aux;
    }
    
    @Override
    public int[] getVectorDecimal() throws Exception{
        
        int cantidad=0;
        int[]arreglo=new int[this.m.length];
        
        if(this.m==null){
            throw new Exception("El vector esta vacio");
        }else{
            for(int i=0; i<this.m.length;i++){
            
            for(int j=(this.m[i].length)- 1; j>=0; j--){
                int aux=0;
               if(this.m[i][j]){
                   cantidad = cantidad+((int)Math.pow(2,aux));
               }
               aux++;
            }
            arreglo[i]=cantidad;
            cantidad=0;
            }
        }
        return arreglo;
    }
    
    public MatrizBinaria getSumar(MatrizBinaria m2)throws Exception{
        MatrizBinaria rta;
        String binarioString="";
        int[]sumas;
        
        if(this.isDispersa() || m2.isDispersa()){
            throw new Exception("No se pueden sumar");
        }else {            
            int[] arreglo1= this.getVectorDecimal();
            int[] arreglo2= m2.getVectorDecimal();
            sumas= new int[this.m.length];
            
            for(int i=0; i<arreglo1.length;i++){
             sumas[i]=arreglo1[i]+arreglo2[i];   
            }
            for(int i=0; i<sumas.length; i++){
                String aux="";
                int numero=sumas[i];
                while(numero>0){
                    
                    if(numero==1){
                        aux="1"+aux; 
                        break; }
                    else{ 
                        aux=","+(numero%2)+aux;
                        numero=numero/2; }
                }
                binarioString=aux+";";
            }
            rta= new MatrizBinaria(binarioString);
        }
        return rta;
    }
}
